var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuarios', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');

        });
    });
});

afterEach(function(done){
    Reserva.deleteMany({}, function(err, success) {
        if(err) console.log(err);
        
        Usuario.deleteMany({}, function (err, success) {
           if(err) console.log(err);
        done();  
        });
    });
});

    describe('Cuando un Usuario reserva una bici', () => {
        it('desde exitir la reserva', (done) => {
            const usuario = new Usuario({nombre:'Luis'});
            usuario.save();
            
            const bicicleta = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta)

            
        });
    });

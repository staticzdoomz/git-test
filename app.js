require('dotenv').config();  
require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter =  require('./routes/api/usuarios');  
var authAPIRouter = require('./routes/api/auth');


var app = express();
var mongoose = require('mongoose');
// var mongoDB = 'mongodb://localhost/red_bicicletas';
//var mongoDB = 'mongodb+srv://admin:3L5cXnscMuMputN@red-bicicletas.si8qj.mongodb.net/<dbname>?retryWrites=true&w=majority';
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console, 'MongoDB connection error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);


app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasAPIRouter); 
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/api/auth', authAPIRouter);

app.use('/privacy_policy', function(req, res){
  res.sendFile('public/policy_privacy.html');
});


app.use('/google4938a48ec1ee2f06.html', function(req, res){
  res.sendFile('public/google4938a48ec1ee2f06.html');
});



app.get('/login', function (req, res) {
  res.render('log/login');
});
app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render('log/login', {
      info
    });
    req.logIn(usuario, function (err) {
      if (err) return next(err)
      return res.redirect('/')
    });
  })(req, res, next);
});

app.get('/logout', function (req, res) {
  req.logOut()
  res.redirect('/');
});

app.get('/forgotPassword', function (req, res) {
  res.render('log/forgotPassword');
});

app.post('/forgotPassword', function (req, res, next) {
  User.findOne({
    email: req.body.email
  }, function (err, user) {
    if (!user) return res.render('log/forgotPassword', {
      info: {
        message: 'No existe el email para un usuario existente'
      }
    });

    user.resetPassword(function (err) {
      if (err) return next(err);
      console.log('log/forgotPasswordMessage');
    });
    res.render('log/forgotPasswordMessage');
  });
});


app.get('/resetPassword/:token', function (req, res, next) {
  console.log(req.params.token);
  token.findOne({
    token: req.params.token
  }, function (err, token) {
    if (!token) return res.status(400).send({
      msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado'
    });
    User.findById(token._userId, function (err, user) {
      if (!user) return res.status(400).send({
        msg: 'No existe un usuario asociado al token.'
      });
      res.render('log/resetPassword', {
        errors: {},
        user: user
      });
    });
  });
});

app.post('/resetPassword', function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render('log/resetPassword', {
      errors: {
        confirm_password: {
          message: 'No coincide con el password ingresado'
        }
      },
      user: new User({
        email: req.body.email
      })
    });
    return;
  }
  User.findOne({
    email: req.body.email
  }, function (err, user) {
    user.password = req.body.password;
    user.save(function (err) {
      if (err) {
        res.render('log/resetPassword', {
          errors: err.errors,
          user: new User({
            email: req.body.email
          })
        });
      } else {
        res.redirect('/login');
      }
    });
  });
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('User sin loguearse');
    res.redirect('/login');
  }
};

function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      console.log('Error en validar Usuario');
      res.json({
        status: "error",
        message: err.message,
        data: null
      });
    } else {
      console.log('Pasó el usuario: ' + req.body.userId);
      req.body.userId = decoded.id;
      console.log('JWT verify: ' + decoded);
      next();
    }
  });
};



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
